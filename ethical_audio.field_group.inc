<?php
/**
 * @file
 * ethical_audio.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ethical_audio_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|file|audio|form';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'file';
  $field_group->bundle = 'audio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Edit details',
    'weight' => '2',
    'children' => array(
      0 => 'field_file_description',
      1 => 'field_file_copyright',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_details|file|audio|form'] = $field_group;

  return $export;
}
